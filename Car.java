public abstract class Car implements IVehicle {
    protected int speed;
    public int getSpeed(){
        return this.speed;
    }
    public String toString(){
        return "The car is moving at speed " + this.speed;
    }
    public Car(int s){
        this.speed = s;
    }

}
