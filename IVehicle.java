public interface IVehicle{
    public void speedUp(int speed);
    public void stop();
}