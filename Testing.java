public class Testing {
    public static void main(String[]args){
        OldCar c = new OldCar(10);
        c.speedUp(3);
        System.out.println(c.getSpeed());
        c.speedUp(2);
        System.out.println(c.getSpeed());
        c.speedUp(5);
        System.out.println(c.getSpeed());
        c.speedUp(-2);
        System.out.println(c.getSpeed());
        c.speedUp(3);
        System.out.println(c.getSpeed());
        c.speedUp(10);
        System.out.println(c.getSpeed());
        c.speedUp(2);
        System.out.println(c.getSpeed());
        c.stop();
        System.out.println(c.getSpeed());
        c.speedUp(2);
        System.out.println(c.getSpeed());
        c.speedUp(4);
        System.out.println(c.getSpeed());
        c.speedUp(5);
        System.out.println(c.getSpeed());


    }
}
