import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestingOldCar {
    @Test
    public void testSpeedUpPositives(){
        OldCar c = new OldCar(10);
        c.speedUp(2); //12
        c.speedUp(3); //15
        c.speedUp(4); //15
        c.speedUp(5); //20
        c.speedUp(6); //26
        c.speedUp(7); //26
        assertEquals(c.getSpeed(), 26);
    }
    @Test
    public void testSpeedUpNegatives(){
        OldCar c = new OldCar(-10);
        c.speedUp(-2); //-12
        c.speedUp(-3); //-15
        c.speedUp(-4); //-15
        c.speedUp(-5); //-20
        c.speedUp(-6); //-26
        c.speedUp(-7); //-26
        assertEquals(c.getSpeed(), -26);
    }
    @Test
    public void testSpeedUpMixed(){
        OldCar c = new OldCar(-5);
        c.speedUp(2); //-3
        c.speedUp(-2); //-5
        c.speedUp(12); //-5
        c.speedUp(-5); //-10
        c.speedUp(16); //6
        c.speedUp(-10); //6
        c.speedUp(-5); //1
        assertEquals(c.getSpeed(), 1);
    }
    @Test
    public void testStop(){
        OldCar neg = new OldCar(-5);
        OldCar pos = new OldCar(5);
        neg.stop();
        pos.stop();
        assertEquals(pos.getSpeed(),0);
        assertEquals(neg.getSpeed(),0);

    }
}


