public class OldCar extends Car {
    private int countingSpeedUps;
    public OldCar(int spd){
        super(spd);
        this.countingSpeedUps=0;


    }
    public void speedUp(int speed){
        final int MAX_SPEEDUPS_BEFORE_STALL = 3;
        this.countingSpeedUps++;
        if(this.countingSpeedUps % MAX_SPEEDUPS_BEFORE_STALL != 0){
           this.speed+=speed;
        }
    }
    public void stop(){
        this.speed=0;
        this.countingSpeedUps=0;
    }
    
}
